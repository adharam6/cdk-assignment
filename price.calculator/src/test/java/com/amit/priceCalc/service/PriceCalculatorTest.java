package com.amit.priceCalc.service;

import static org.junit.Assert.*;

import org.junit.Test;

public class PriceCalculatorTest {
/**
 * Test cases to test price calculation and discount
 */
	@Test
	public void testCalculatePrice() {
		PriceCalculator priceCalculator = new PriceCalculator();
		int actual = priceCalculator.calculatePrice(5000, "Regular");
		assertEquals(5000, actual);
		
		actual = priceCalculator.calculatePrice(5000, "Regular");
		assertEquals(5000, actual);
		
		actual = priceCalculator.calculatePrice(10000, "Regular");
		assertEquals(9500, actual);
		
		actual = priceCalculator.calculatePrice(15000, "Regular");
		assertEquals(13500, actual);
		
		actual = priceCalculator.calculatePrice(4000, "Premium");
		assertEquals(3600, actual);
		
		actual = priceCalculator.calculatePrice(8000, "Premium");
		assertEquals(7000, actual);
		
		actual = priceCalculator.calculatePrice(12000, "Premium");
		assertEquals(10200, actual);
		
		actual = priceCalculator.calculatePrice(20000, "Premium");
		assertEquals(15800, actual);
	}

}
