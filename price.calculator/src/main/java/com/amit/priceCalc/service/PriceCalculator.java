package com.amit.priceCalc.service;

import org.springframework.stereotype.Service;

/**
 * This is one of the implementation logic of price calculation. We can Implement PriceCalcImpl for other client or for other logic in different class
 * @author USER
 *
 */

@Service
public class PriceCalculator implements PriceCalcImpl {

	@Override
	public Integer calculatePrice(Integer price, String customerType) {
		if(customerType.equals("Regular")) {
			if(price <= 5000) {
				return price;
			}else if(price <= 10000) {
				return price - getPercent(10,price - 5000);
			}else if(price > 10000) {
				return price - getPercent(10,5000) - getPercent(20,price - 10000);
			}else {
				System.out.println("Invalid Price");
				return null;
			}
		}else if(customerType.equals("Premium")) {
			if(price >0 && price <= 4000) {
				return price - getPercent(10,price);
			}else if(price <= 8000) {
				return price - getPercent(10,4000) - getPercent(15,price - 4000);
			}else if(price <= 12000) {
				return price - getPercent(10,4000) - getPercent(15,4000) - getPercent(20,price - 8000);
			}else if (price > 12000){
				return price - getPercent(10,4000) - getPercent(15,4000) - getPercent(20,4000) - getPercent(30,price - 12000);
			}else {
				System.out.println("Invalid Price");
				return null;
			}
		}else {
			System.out.println("Invalid Customer");
			return null;
		}
	}
	
	private Integer getPercent(int noOfPercent, int value) {
		return value * noOfPercent / 100;
	}

}
