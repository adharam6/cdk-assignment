package com.amit.priceCalc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PriceCalcService {

	@Autowired
	PriceCalculator priceCalculator;
	
	public Integer calculateDiscountedPrice(Integer price, String customerType) {
		return priceCalculator.calculatePrice(price, customerType);
	}

}
