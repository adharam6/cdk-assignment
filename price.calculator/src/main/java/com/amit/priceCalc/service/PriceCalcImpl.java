package com.amit.priceCalc.service;

public interface PriceCalcImpl {

	public Integer calculatePrice(Integer price,String customerType);
}
