package com.amit.priceCalc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amit.priceCalc.service.PriceCalcService;

@RestController
@RequestMapping("/api/v1/onlineStore")
public class PriceCalcController {

	@Autowired
	PriceCalcService priceCalcService;
	
	@GetMapping("/calculateDiscountedPrice")
	public @ResponseBody Integer calculateDiscountedPrice(@RequestParam Integer price, @RequestParam String customerType) {
		return priceCalcService.calculateDiscountedPrice(price,customerType);
	}
	
}
